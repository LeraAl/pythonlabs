SMARTRM

Author: Aleshko Valeria
E-mail: aleshko.valery@gmail.com
Version: 1.0


Config rules

Clean Policy

- Time

You can set time, and files that are in trash for a longer period will be deleted when you start the program
Time must be witten as "1d 2h 3m"
You can set any number of days, hours or minutes.
For example:
Time=0d 0h 10m
Time=0d 2h 0m
Time=3d 2h 0m

- Size

You can set size and when your trash_size will be more then setted, old files will be removed.
Files are removing from old to new while trash_size s bigger than setted
You must set size in Mb and kb
For example:
Size=1Mb 0kb
Size=0Mb 4kb
Size=12Mb 3kb


Replace Policy

ReplaceWhenRemove
This option can be setted as 'Yes' or 'No'
Yes: If you delete file with the same name as file existing in trash it will be renamed to store.
File restore with its original name
For example:
ReplaceWhenRemove=Yes

ReplaceWhenRestore
This option can be setted as 'Yes' or 'No'
Yes: If you restore file with the same name as file existing in path_to_restore it will be renamed.
For example:
ReplaceWhenRemove=No