#!/usr/bin/python2.7

from setuptools import setup, find_packages
import smartrm


setup(
    name='smartrm',
    version='1.0',
    packages=find_packages(),
    url='',
    license='',
    author='Valeria Aleshko',
    author_email='aleshko.valery@gmail.com',
    test_suite="tests",
    entry_points={
        'console_scripts':['smrm = smartrm.parser:smrm_main', 'smtrash = smartrm.parser:smtrash_main']
    }
)
