class FileNotFoundError(OSError):
    def __init__(self, filename=None):
        self.filename = filename


class PermissionError(OSError):
    def __init__(self, filename):
        self.filename = filename


class DirectoryIsNotEmpty(OSError):
    def __init__(self, filename):
        self.filename = filename


class RemoveTrashDirectoryError(OSError):
    def __init__(self, filename):
        self.filename = filename


class TooManyFilesFound(OSError):
    def __init__(self, filename):
        self.filename = filename