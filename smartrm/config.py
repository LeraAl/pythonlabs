import re
from datetime import timedelta, datetime
import os.path


class Config(object):
    def __init__(self, path_to_config=None):
        self._trash_path = os.path.expanduser('~/.trash')

        self._time_before_cleaning = timedelta(days=1)

        self._size_to_clean = None

        self._replace_when_remove = False
        self._replace_when_restore = True

        self._logging = False

        if path_to_config is not None:
            config_params = Config.parse_config_file(path_to_config)
            self.parse_params(config_params)

    @staticmethod
    def parse_config_file(path_to_config):
        dict_config = dict()
        with open(path_to_config, 'r') as config_file:
            for line in config_file.readlines():
                if re.match('\[.*\]', line):
                    continue
                if re.match('#.*', line):
                     continue
                if not line.strip():
                    continue
                else:
                    key, value = line.strip().split('=')
                    dict_config[key] = value
        return dict_config

    def parse_params(self, params):
        trash_path = params.get('Path', None)
        if not trash_path:
            self._trash_path = trash_path

        time = params.get('Time', None)
        if time:
            parsed_time = datetime.strptime(time, "%dd %Hh %Mm")
            self._time_before_cleaning = timedelta(days=parsed_time.day, )

        size_to_clean = params.get('Size', None)
        if size_to_clean == 'None':
            size_to_clean = None
        elif size_to_clean:
            parsed_size = re.match(r"(?P<megabytes>\d+)Mb (?P<kilobytes>\d+)kb", size_to_clean)
            self._size_to_clean = int(parsed_size.group('megabytes')) * 1024 + int(parsed_size.group('kilobytes'))

        if params.get('ReplaceWhenRemove') == 'Yes':
            self._replace_when_remove = True

        if params.get('ReplaceWhenRestore') == 'Yes':
            self._replace_when_remove = True

        if params.get('LoggingOn') == 'Yes':
            self._logging = True

    @staticmethod
    def parse_time(time_string):
        if time_string == 'None':
            return None
        parsed_time = re.match(r"(?P<days>\d+)d (?P<hours>\d+)h (?P<minutes>\d+)m", time_string)
        if parsed_time:
            time_delta = datetime.timedelta(days=int(parsed_time.group('days')),
                                            hours=int(parsed_time.group('hours')),
                                            minutes=int(parsed_time.group('minutes')))
        else:
            time_delta = None
        return time_delta

    def get_params(self):
        params = dict()
        params['trash_path'] = self._trash_path
        params['time_before_cleaning'] = self._time_before_cleaning
        params['size_to_clean'] = self._size_to_clean
        params['replace_when_remove'] = self._replace_when_remove
        params['replace_when_restore'] = self._replace_when_restore
        return params

    @property
    def is_logged(self):
        return self._logging

    @property
    def trash_path(self):
        return self._trash_path

    @property
    def time_before_cleaning(self):
        return self._time_before_cleaning

    @property
    def size_to_clean(self):
        return self._size_to_clean

    @property
    def replace_when_remove(self):
        return self._replace_when_remove

    @property
    def replace_when_restore(self):
        return self._replace_when_restore
