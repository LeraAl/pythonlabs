import os
import time


class File(object):
    def __init__(self, path):
        self.abspath = os.path.abspath(path)
        self.path = os.path.relpath(path)
        self.name = os.path.basename(path)

    def is_empty(self):
        if self.is_dir():
            return len(os.listdir(self.abspath)) == 0
        else:
            return False

    def is_exist(self):
        return os.path.exists(self.abspath)

    def is_link(self):
        return os.path.islink(self.abspath)

    def is_dir(self):
        return os.path.isdir(self.abspath)

    def is_file(self):
        return os.path.isfile(self.abspath)

    @staticmethod
    def get_last_modified_time():
        current_time = time.localtime()
        return time.strftime('%d.%m.%Y %H:%M:%S', current_time)