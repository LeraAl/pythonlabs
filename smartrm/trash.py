import os
import shutil
import csv
from datetime import datetime
import logging
import re
import threading
import sre_constants
from smartrm.file import File
from exceptions import *


class Trash(object):

    time_format = '%d.%m.%Y %H:%M:%S'

    def __init__(self, trash_path,
                 replace_when_remove=False,
                 replace_when_restore=True,
                 dry_run_rm_decorator=None,
                 dry_run_restore_decorator=None,
                 regex=False,
                 confirm_remove=lambda x: True,
                 confirm_go_into=lambda x: False,
                 error_handler=None,
                 time_before_cleaning=None,
                 size_to_clean=None,
                 output_function=lambda x: None,
                 logger=None):
        """
        Initialisation Trash object

        :param trash_path: string with path where trash will be situated
        :param replace_when_remove: boolean parameter
        :param replace_when_restore: boolean parameter
        :param dry_run_rm_decorator: decorates self._remove_file function in dry_run mode
        :param dry_run_restore_decorator: decorates self._restore_file function in dry_run mode
        :param regex: boolean parameter. If True functions check all filenames as regex
        :param confirm_remove: function that asked user before removing. Return: boolean. Default: lambda x: True
        :param confirm_go_into: function that asked user for descending to folder. Return: boolean. Default: lambda x: False
        :param error_handler: function that handle errors
            - Errors to handle(from smartrm.exceptions):
            - FileNotFoundError
            - PermissionError
            - DirectoryIsNotEmpty
            - RemoveTrashDirectoryError
            - TooManyFilesFound

        :param time_before_cleaning: timedelta paramter. Files that are in trash for longer time will be deleted
        :param size_to_clean: integer parameter. If size of trash is more than this value(in bytes) old files will be deleted
        :param logger: logger
        :param output_function: function to output information

        """
        if logger is None:
            logger = logging.getLogger('trash_logger')
            logger.disabled = True
        self.logger = logger

        self.logger.debug("Path: {}".format(trash_path))

        self.trash_path = os.path.join(trash_path, 'files/')
        self.__init_trash_dir__(trash_path)

        self.replace_when_remove = replace_when_remove
        self.replace_when_restore = replace_when_restore
        self.time_before_cleaning = time_before_cleaning
        self.size_to_clean = size_to_clean
        self.regex = regex
        self.removed_files = None
        print self.removed_files

        self.confirm_remove = confirm_remove
        self.confirm_go_into = confirm_go_into
        self.error_handler = error_handler
        self.output = output_function
        if dry_run_rm_decorator is not None:
            self._move_file_to_trash = dry_run_rm_decorator(self._move_file_to_trash)
        if dry_run_restore_decorator is not None:
            self._restore_file = dry_run_restore_decorator(self._restore_file)

        self.__check_missing_files()
        #self.clean()

        self.logger.info("OK")

    def __init_trash_dir__(self, trash_path):
        """Make trash dir if it does not exist"""
        if not os.path.exists(self.trash_path):
            os.makedirs(self.trash_path)
        self.trash_info = os.path.abspath(os.path.join(trash_path, 'info/'))

        if not os.path.exists(self.trash_info):
            os.makedirs(self.trash_info)
        self.trash_info_file = os.path.join(self.trash_info, 'info.csv')
        self.trash_info_file_header = ['Name', 'Name in trash', 'Delete time', 'Path to restore']

        if not os.path.exists(self.trash_info_file):
            with open(self.trash_info_file, 'w') as f:
                writer = csv.DictWriter(f, fieldnames=self.trash_info_file_header)
                writer.writeheader()

    def __check_missing_files(self):
        """__check__missing_files(self) -> integer

        :return count of files that don't exist in trash, but are in info.csv

        remove rows from info.csv with non-existing files

        """
        with open(self.trash_info_file, 'r') as csv_file:
            reader = csv.DictReader(csv_file)
            files_info = [row for row in reader]

        deleted_files_count = 0
        for file_info in files_info[:]:
            file_path_in_trash = os.path.join(self.trash_path, file_info['Name in trash'])

            if not os.path.exists(file_path_in_trash):
                deleted_files_count += 1
                files_info.remove(file_info)

        with open(self.trash_info_file, 'w') as f:
            writer = csv.DictWriter(f, self.trash_info_file_header)
            writer.writeheader()
            writer.writerows(files_info)

        return deleted_files_count

    def remove(self, files):
        """
        remove files

        :return 0 if no errors, 1 otherwise

        """
        self.logger.debug("Files: {}".format(files))

        code_to_return = 0
        self.removed_files = set()
        if self.regex:
            files_in_dir = [os.path.abspath(i) for i in os.listdir('./')]
            files, code_to_return = self._match_regex(files_in_dir, files)
        threads = []
        for filename in files:
            try:
                file = File(filename)

                if os.path.commonprefix([self.trash_path, file.abspath]) == file.abspath:
                    raise RemoveTrashDirectoryError(filename=file.abspath)

                if not file.is_exist():
                    raise FileNotFoundError(file.path)

                if not os.access(file.abspath, os.W_OK):
                    raise PermissionError(filename=file.path)

                if file.is_link() or file.is_file():
                    thread = threading.Thread(target=self.remove_file, args=(file,))
                    thread.start()
                    threads.append(thread)

                elif file.is_dir():
                    thread = threading.Thread(target=self.remove_dir, args=(file,))
                    thread.start()
                    threads.append(thread)

            except FileNotFoundError as err:
                self.logger.error("File not found: {}".format(err.filename))

                code_to_return = 1
                if self.error_handler is not None:
                    self.error_handler(err)
                else:
                    raise

            except PermissionError as err:
                self.logger.error("Permission denied: {}".format(err.filename))

                code_to_return = 1
                if self.error_handler is not None:
                    self.error_handler(err)
                else:
                    raise

            except RemoveTrashDirectoryError as err:
                self.logger.error("Removing under-trash directory: {}".format(err.filename))

                code_to_return = 1
                if self.error_handler is not None:
                    self.error_handler(err)
                else:
                    raise
        
        for thread in threads:
            thread.join()
        
        self.logger.info("OK")
        self.logger.debug('removed files {}'.format(self.removed_files))
        return self.removed_files

    def remove_file(self, file):
        """
        :param file: File object to remove
        """
        self.logger.debug(file.path)

        if self.confirm_remove(file.path):
            self._move_file_to_trash(file)

        self.logger.info("OK")

    def remove_dir(self, directory):
        self.logger.debug("Directory: {}".format(directory.path))

        code_to_return = 0
        if not directory.is_empty() and self.confirm_remove != (lambda x: False):
            files_to_remove, code_to_return = self._choose_files_to_remove(os.getcwd(), directory.path)
            if files_to_remove:
                self._move_list_to_trash(files_to_remove)
            self.logger.info("OK")
            return code_to_return

        try:
            if self.confirm_remove(directory.path):
                if not directory.is_empty():
                    raise DirectoryIsNotEmpty(directory.path)
                self._move_file_to_trash(directory)
        except DirectoryIsNotEmpty as err:
            self.logger.error("Directory is not empty: {}".format(err.filename))

            code_to_return = 1
            if self.error_handler is not None:
                self.error_handler(err)
            else:
                raise

        self.logger.info("OK")
        return code_to_return

    def _choose_files_to_remove(self, root, subdir):
        """
        _choose_files_to_remove(self, root, subdir) -> list

        Interactively(asking user) choose files in subdir
        return list of paths to files that should to delete
        path is relative

        """
        self.logger.debug("root: {}, subdir: {}".format(root, subdir))
        code_to_return = 0

        self.logger.debug("dir: {} {}".format(os.path.abspath(subdir), os.access(os.path.abspath(subdir), os.W_OK)))
        if not os.access(os.path.abspath(subdir), os.W_OK):
            return [], 1

        files_to_remove = set()
        subdir_relpath = os.path.relpath(subdir, root)
        self.logger.debug("subdir_relpath: {}".format(subdir_relpath))
        
        content_paths = [os.path.join(subdir_relpath, file) for file in os.listdir(subdir)]
        self.logger.debug("content_paths: {}".format(content_paths))

        if content_paths and self.confirm_go_into(subdir):
            for file_path in content_paths:
                file = File(file_path)
                if file.is_file() or file.is_link():
                    if self.confirm_remove(file.path):
                        try:
                            self.logger.debug("abspath: {}".format(file.abspath))
                            self.logger.debug("os.access: {}".format(os.access(file.abspath, os.W_OK)))
                            
                            if not os.access(file.abspath, os.W_OK):
                                raise PermissionError(filename=file.path)
                            files_to_remove.add(file.path)
                        except PermissionError as err:
                            self.logger.error("Permission denied: '{}'".format(file.path))
                            
                            self.error_handler(err)
                            code_to_return = 1
                            return [], code_to_return

                else:
                    self.logger.debug("dir: {} {}".format(os.path.abspath(file_path), os.access(os.path.abspath(file_path), os.W_OK)))
                    if not os.access(os.path.abspath(file_path), os.W_OK):
                        return [], 1
                    inner_files, code_to_return = self._choose_files_to_remove(root, file_path)
                    if code_to_return == 1:
                        return [], 1
                    files_to_remove.update(inner_files)
        try:
            if self.confirm_remove(subdir_relpath):
                if files_to_remove.issuperset(set(content_paths)):
                    files_to_remove.clear()
                    files_to_remove.add(subdir)
                else:
                    raise DirectoryIsNotEmpty(filename=subdir_relpath)
        except DirectoryIsNotEmpty as err:
            self.logger.error("Directory is not empty: {}".format(err.filename))

            code_to_return = 1
            if self.error_handler is not None:
                self.error_handler(err)
            else:
                raise

        self.logger.info("OK")
        return files_to_remove, code_to_return

    def _move_list_to_trash(self, files):
        self.logger.debug("files: {}".format(files))

        for file_name in files:
            file = File(file_name)
            self._move_file_to_trash(file)

        self.logger.info("OK")

    def _move_file_to_trash(self, file):
        self.logger.debug("file: {}".format(file.path))

        with open(self.trash_info_file, 'r') as csv_file:
            reader = csv.DictReader(csv_file)
            files_info = [row for row in reader]

        filename_in_trash = file.name
        path_to_move = os.path.join(self.trash_path, filename_in_trash)
        if os.path.exists(path_to_move + '.1'):
            if not self.replace_when_remove:
                i = 2
                while os.path.exists(path_to_move + '.' + str(i)):
                    i += 1
                path_to_move = path_to_move + '.' + str(i)
                filename_in_trash = filename_in_trash + '.' + str(i)
            else:
                same_file = filter(lambda file_info: file_info['Name'] == filename_in_trash, files_info)
                if same_file:
                    # replace the last deleted file
                    file_path = os.path.join(self.trash_path, same_file[-1]['Name in trash'])

                    if os.path.isdir(file_path):
                        shutil.rmtree(file_path)
                    else:
                        os.remove(file_path)

                    files_info.remove(*same_file)
                    with open(self.trash_info_file, 'w') as f:
                        writer = csv.DictWriter(f, self.trash_info_file_header)
                        writer.writeheader()
                        writer.writerows(files_info)
        else:
            filename_in_trash = file.name + '.1'
            path_to_move = path_to_move + '.1'

        data = {'Name': file.name, 'Name in trash': filename_in_trash, 'Delete time': file.get_last_modified_time(),
                'Path to restore': file.abspath}
        
        os.rename(file.abspath, path_to_move)
        self.removed_files.add(file.abspath)

        with open(self.trash_info_file, 'a') as f:
            writer = csv.DictWriter(f, self.trash_info_file_header)
            writer.writerow(data)

        self.logger.info("OK")
    
    def restore(self, files, to_current_dir=False):
        """
        :param files: list of filenames to restore from trash
        :param to_current_dir: boolean parameter. If True restore to current dir, if False restore to old filepath
        :return: 0 if no errors, 1 otherwise

        """
        self.logger.debug('files: {}'.format(files))

        code_to_return = 0
        with open(self.trash_info_file, 'r') as csv_file:
            reader = csv.DictReader(csv_file)
            files_info = [row for row in reader]

        for filename in files:
            try:
                files_info_to_restore = filter(lambda file_info: file_info['Name'] == filename, files_info)
                if not files_info_to_restore:
                    files_info_to_restore = filter(lambda file_info: file_info['Name in trash'] == filename, files_info)
                    if not files_info_to_restore:
                        raise FileNotFoundError(filename)
                    else:
                        # list files_info_to_restore includes only one item
                        self._restore_file(*files_info_to_restore)
                        files_info.remove(*files_info_to_restore)

                elif len(files_info_to_restore) > 1:
                    raise TooManyFilesFound(filename=filename)

                else:
                    self._restore_file(*files_info_to_restore)
                    files_info.remove(*files_info_to_restore)

            except FileNotFoundError as err:
                code_to_return = 1
                if self.error_handler is not None:
                    self.error_handler(err)
                else:
                    raise
            except TooManyFilesFound as err:
                code_to_return = 1
                if self.error_handler is not None:
                    self.error_handler(err)
                else:
                    raise

        with open(self.trash_info_file, 'w') as f:
            writer = csv.DictWriter(f, self.trash_info_file_header)
            writer.writeheader()
            writer.writerows(files_info)

        self.logger.info("OK")
        return code_to_return

    def _restore_file(self, file_info, to_current_dir=False):
        self.logger.debug('file_info: {}'.format(file_info))

        path_to_restore_from = os.path.join(self.trash_path, file_info['Name in trash'])
        if to_current_dir:
            path_to_restore = os.path.join(os.getcwd(), file_info['Name'])
        else:
            path_to_restore = file_info['Path to restore']

        if os.path.exists(path_to_restore):
            if not self.replace_when_restore:
                i = 2
                while os.path.exists(path_to_restore + '.' + str(i)):
                    i += 1
                path_to_restore = path_to_restore + '.' + str(i)
            else:
                if os.path.isdir(path_to_restore):
                    shutil.rmtree(path_to_restore)
                elif os.path.isfile(path_to_restore):
                    os.remove(path_to_restore)

        os.rename(path_to_restore_from, path_to_restore)

        self.logger.info("OK")

    def show(self, long_format=False, need_sort=False, regex=None):
        """
        show files with output function

        :param long_format: if True output filename, filename in trash, path to restore, deleted time,
        if False output only filenames
        :param need_sort: if True output sorted info
        :param regex: output only files that match regex

        """
        self.logger.debug('')
        with open(self.trash_info_file, 'r') as csv_file:
            reader = csv.DictReader(csv_file)
            files_info = [row for row in reader]

        if regex:
            files_info, code_to_restore = self._match_regex(files_info, regex)

        if need_sort:
            files_info.sort(key=lambda file_info: file_info['Name'])

        for file_info in files_info:
            name = file_info.get('Name')
            if long_format:
                name_in_trash= file_info.get('Name in trash')
                path_to_restore = file_info.get('Path to restore')
                delete_time = file_info.get('Delete time')
                self.output('{:<10} {:<10} {:<40} {:<20}'.format(name, name_in_trash, path_to_restore, delete_time))
            else:
                self.output(name)

        self.logger.info("OK")

    def clean(self, clean_all=False):
        """
        clean trash for time policy and size policy

        :param clean_all: if True clean all trash

        """
        self.logger.debug('{} {}'.format(self.size_to_clean, self.size_to_clean is not None))

        dir_to_return = os.getcwd()
        os.chdir(self.trash_path)

        if clean_all:
            for file in os.listdir('./'):
                self._delete_file(file)
        else:
            with open(self.trash_info_file, 'r') as csv_file:
                reader = csv.DictReader(csv_file)
                files_info = [row for row in reader]
                for file_info in files_info:
                    file_info['Delete time'] = datetime.strptime(file_info['Delete time'], Trash.time_format)

            if self.size_to_clean is not None:
                files_info = self._delete_by_size(files_info)

            if self.time_before_cleaning is not None:
                files_info = self._delete_by_time(files_info)

        os.chdir(dir_to_return)
        deleted_files_count = self.__check_missing_files()
        if deleted_files_count != 0:
            self.output("{} files was removed from trash.".format(deleted_files_count))

        self.logger.info("OK")

    def _delete_by_size(self, files_info):
        self.logger.debug([self._get_size(), self.size_to_clean])

        files_info = sorted(files_info, key=lambda file_info: file_info['Delete time'])

        while self._get_size() > self.size_to_clean:
            if not files_info:
                break
            else:
                file_to_delete = os.path.join(self.trash_path, files_info[0]['Name in trash'])
                self._delete_file(file_to_delete)
                files_info.remove(files_info[0])

        self.logger.info("OK")
        return files_info

    def _delete_by_time(self, files_info):
        self.logger.debug(self.time_before_cleaning)
        files_info = sorted(files_info, key=lambda file_info: file_info['Delete time'])

        while files_info:
            delete_time = files_info[0]['Delete time']
            self.logger.debug([datetime.now(), delete_time, datetime.now() - delete_time ])
            if datetime.now() - delete_time > self.time_before_cleaning:
                file_to_delete = os.path.join(self.trash_path, files_info[0]['Name in trash'])
                self._delete_file(file_to_delete)
                files_info.remove(files_info[0])
            else:
                break

        self.logger.info("OK")
        return files_info

    def _delete_file(self, filename):
        self.logger.debug(filename)

        if os.path.isdir(filename):
            shutil.rmtree(filename)
        elif os.path.isfile(filename):
            os.remove(filename)

        self.logger.info("OK")

    def _get_size(self):
        self.logger.debug(os.listdir('./'))
        total_size = 0
        for dir_path, dir_names, file_names in os.walk(self.trash_path):
            for f in file_names:
                file_path = os.path.join(dir_path, f)
                total_size += os.path.getsize(file_path)

        self.logger.info([total_size, "OK"])
        return total_size

    def _match_regex(self, files, reg_expressions):
        self.logger.debug('files: {}, regex: {}'.format(files, reg_expressions))

        code_to_return = 0
        filtered_list = []
        try:
            for regex in reg_expressions:
                if isinstance(files[0], dict):
                    filtered_list.extend([file_info for file_info in files if re.match(regex + '$', file_info.get('Name in trash'))])
                elif isinstance(files[0], str):
                    filtered_list.extend([file_name for file_name in files if re.match(regex + '$', file_name)])
        except sre_constants.error as err:
            code_to_return = 1
            if self.error_handler is not None:
                self.error_handler(err)
            else:
                raise

        self.logger.info("OK")
        return filtered_list, code_to_return
