#!/usr/bin/python2.7

import argparse
import logging
import re
import sys
import os
import datetime
from smartrm.trash import Trash
import smartrm.handlers as handlers
from smartrm.config import Config


def smrm_main():
    parser = argparse.ArgumentParser()
    add_arguments_to_parser(parser)

    args = parser.parse_args()

    if args.config:
        config = Config(args.config[0])
    else:
        config = Config()

    params = merge_params(args, config.get_params())

    FORMAT = "%(asctime)s - %(levelname)-8s: %(filename)-10s: %(funcName)s:  %(message)s "
    log_file = args.log_file if args.log_file else '/var/log/smartrm.log/'
    logging.basicConfig(format=FORMAT, filename=log_file, level=logging.DEBUG)
    logger = logging.getLogger('smrm_logger')

    if not args.logging and not config.is_logged and args.log_file is None:
        logger.disabled = True

    if args.force:
        error_handler = handlers.error_handler_forced
    elif args.silent_mode:
        error_handler = handlers.error_handler_pass
    else:
        error_handler = handlers.error_handler

    if args.interactive:
        confirm_remove = handlers.confirm_remove
    else:
        confirm_remove = lambda x: True

    if args.interactive and args.recursive:
        confirm_go_into = handlers.confirm_go_into
    elif args.recursive:
        confirm_go_into = lambda x: True
    else:
        confirm_go_into = lambda x: False

    if args.dry_run:
        dry_run_decorator = handlers.dry_run_remove_decorator
    else:
        dry_run_decorator = None



    trash = Trash(
        logger=logger,
        dry_run_rm_decorator=dry_run_decorator,
        regex=args.regex,
        confirm_remove=confirm_remove,
        confirm_go_into=confirm_go_into,
        error_handler=error_handler,
        output_function=handlers.output_message,
        **params)

    try:
        exit_code = trash.remove(args.files)
    except Exception as err:
        logging.error(err.args)
        if args.silent_mode:
            sys.exit(1)
        raise

    if args.silent_mode:
        sys.exit(exit_code)


def add_arguments_to_parser(parser):
    parser.add_argument('files', action='store', nargs='+')
    group = parser.add_mutually_exclusive_group()
    group.add_argument('-i', '--interactive', action='store_true', help="prompt before every removal")
    group.add_argument('-s', '--silent-mode', action='store_true', help="not output anything")
    group.add_argument('-f', '--force', action='store_true', help="ignore files which are not exist")

    parser.add_argument('-r', '--recursive', action='store_true', help="recursively remove content of directories")
    parser.add_argument('--regex', action='store_true', help="turn on regular expressions")
    parser.add_argument('--logging', action='store_true', help='log in file')
    parser.add_argument('--config', action='store', nargs=1, help="sets path to user config")
    parser.add_argument('--dry-run', action='store_true', help="start program in virtual mode")
    parser.add_argument('--log-file', action='store', default='/home/lera/Documents/pythonlabs/smartrm/smrm.log',
                        help="set file to write log in")

    parser.add_argument('--trash_path', action='store', default=None, nargs=1, type=str,
                        help="set replace policy(yes/No)")
    parser.add_argument('--replace_when_remove', action='store', default=None, nargs=1,
                        help="set replace policy(yes/No)")
    parser.add_argument('--replace_when_restore', action='store', default=None, nargs=1,
                        help="set replace policy(Yes/no)")
    parser.add_argument('--size_to_clean', action='store', default=None, help="set size of trash to clean (in kb)")
    parser.add_argument('--time_to_clean', action='store', default=None,
                        help="files that are in trash for a longer time will be removed"
                             "must use such format '1d 2h 3m'(days, hours, minutes)")


def merge_params(args, config):
    if args.replace_when_remove is not None:
        if args.replace_when_remove[0].lower() == 'yes':
            config['replace_when_remove'] = True
        elif args.replace_when_remove[0].lower() == 'no':
            config['replace_when_remove'] = False

    if args.replace_when_restore is not None:
        if args.replace_when_restore[0].lower() == 'yes':
            config['replace_when_restore'] = True
        elif args.replace_when_restore[0].lower() == 'no':
            config['replace_when_restore'] = False

    if args.size_to_clean:
        config['size_to_clean'] = int(args.size_to_clean)

    if args.time_to_clean:
        parsed_time = re.match(r"(?P<days>\d+)d (?P<hours>\d+)h (?P<minutes>\d+)m", args.time_to_clean)
        if parsed_time:
            time_delta = datetime.timedelta(days=int(parsed_time.group('days')), hours=int(parsed_time.group('hours')),
                                            minutes=int(parsed_time.group('minutes')))
            config['time_before_cleaning'] = time_delta

    if args.trash_path is not None:
        config['trash_path'] = args.trash_path[0]

    return config


def smtrash_main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--logging', action='store_true')
    parser.add_argument('--dry-run', action='store_true')
    parser.add_argument('--config', action='store', nargs=1)

    parser.add_argument('--trash_path', action='store', default=None, nargs=1, type=str,
                        help="set replace policy(yes/No)")
    parser.add_argument('--replace_when_remove', action='store', default='no', nargs=1,
                        help="set replace policy(yes/No)")
    parser.add_argument('--replace_when_restore', action='store', default='yes', nargs=1,
                        help="set replace policy(Yes/no)")
    parser.add_argument('--size_to_clean', action='store', default=None, help="set size of trash to clean (in kb)")
    parser.add_argument('--time_to_clean', action='store', default=None,
                        help="files that are in trash for a longer time will be removed"
                             "must use such format '1d 2h 3m'(days, hours, minutes)")

    subparsers = parser.add_subparsers()

    show_parser = subparsers.add_parser('show', help='Show files in trash')
    show_parser.add_argument('-l', '--long', help='Use a long listing format', action='store_true')
    show_parser.add_argument('-s', '--sorted', help='Show sorted list', action='store_true')
    show_parser.add_argument('-r', '--regex', action='store', nargs='+')
    show_parser.set_defaults(func=show_parse)

    restore_parser = subparsers.add_parser('restore', help='Restore files from trash')
    restore_parser.add_argument('files', action='store', nargs='+')
    restore_parser.add_argument('-c', '--current', help='Restore file in current directory', action='store_true')
    restore_parser.set_defaults(func=restore_parse)

    clean_parser = subparsers.add_parser('clean', help='Clean trash')
    clean_parser.add_argument('-a', '--all', help='Clean all trash', action='store_true')
    clean_parser.set_defaults(func=clean_parse)

    args = parser.parse_args()

    if args.config:
        config = Config(args.config[0])
    else:
        config = Config()

    params = merge_params(args, config.get_params())

    FORMAT = "%(asctime)s - %(levelname)-8s: %(filename)-10s: %(funcName)s:  %(message)s "
    logging.basicConfig(format=FORMAT, filename="/home/lera/Documents/pythonlabs/smartrm/smrm.log",
                        level=logging.DEBUG)
    logger = logging.getLogger('smtrash_logger')

    if not args.logging:
        logger.disabled = True

    if args.dry_run:
        restore_decorator = handlers.dry_run_restore_decorator
    else:
        restore_decorator = None

    trash = Trash(logger=logger, dry_run_restore_decorator=restore_decorator, error_handler=handlers.error_handler, output_function=handlers.output_message, **params)
    args.func(args, trash)


def restore_parse(args, trash):
    trash.restore(args.files, args.current)


def show_parse(args, trash):
    trash.show(long_format=args.long, need_sort=args.sorted, regex=args.regex)


def clean_parse(args, trash):
    trash.clean(clean_all=args.all)
