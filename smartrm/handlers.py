import os
import re, sre_constants
from smartrm.exceptions import *


def confirm_remove(file):
    file_type = ''
    if os.path.islink(file):
        file_type = "symbolyc link"
    elif os.path.isdir(file):
        if os.listdir(os.path.abspath(file)):
            file_type = "directory"
        else:
            file_type = "empty directory"
    elif os.path.isfile(file):
        file_type = "file"
    answer = raw_input("Remove {} '{}'?".format(file_type, file)).lower()
    return answer and answer[0] == 'y'


def confirm_go_into(directory):
    answer = raw_input("Go into '{}'?".format(directory)).lower()
    return answer and answer[0] == 'y'


def output_message(msg):
    print msg


def dry_run_remove_decorator(function):
    def decorator(file):
        print "'{}' was removed".format(file.path)
    return decorator


def dry_run_restore_decorator(function):
    def decorator(file_info):
        print "'{}' was restored".format(file_info['Name'])
    return decorator


# for exceptions which mustn't crash program,
# but must response immediately
def error_handler(err):
    if isinstance(err, FileNotFoundError):
        print "No such file or directory: {}".format(err.filename)

    elif isinstance(err, DirectoryIsNotEmpty):
        print "Directory is not empty: {}".format(err.filename)

    elif isinstance(err, PermissionError):
        print "Permission denied: {}".format(err.filename)

    elif isinstance(err, sre_constants.error):
        print "Invalid regex"

    elif isinstance(err, TooManyFilesFound):
        print "There are more than one file with that name: {}".format(err.filename)
        print "Please write name used in trash"
        print "'smtrash show -l' to show them. Use regex for comfortable searching"

    elif isinstance(err, RemoveTrashDirectoryError):
        print "Can't remove: {}".format(err.filename)
        print "It is parent directory"


def error_handler_forced(err):
    if isinstance(err, DirectoryIsNotEmpty):
        print "Directory is not empty: {}".format(err.filename)


def error_handler_pass(err):
    pass