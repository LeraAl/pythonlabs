import unittest
import os
import logging
import shutil
import subprocess
import smartrm.handlers as hndl
from smartrm.trash import Trash


class FlagsTestClass(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        FORMAT = "%(asctime)s - %(levelname)-8s: %(filename)-10s: %(funcName)s:  %(message)s "
        logging.basicConfig(format=FORMAT, filename="/home/lera/Documents/pythonlabs/smartrm/tests.log",
                            level=logging.DEBUG)
        cls.logger = logging.getLogger()
        cls.logger.debug('')

        cls.trash_dir = os.path.join(os.getcwd(), 'test_trash')
        cls.test_dir = os.path.join(os.getcwd(), 'test_tempdir')
        cls.logger.info("OK")

    def setUp(self):
        self.logger.debug('')

        os.mkdir(self.test_dir)
        os.chdir(self.test_dir)
        with open('f2', 'w'):
            pass
        with open('f2', 'w'):
            pass
        os.chdir('../')

        self.logger.info("OK")

    def tearDown(self):
        self.logger.debug('')

        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)
        shutil.rmtree(self.trash_dir)

        self.logger.info("OK")

    def test_recursive(self):
        self.logger.debug('')

        subprocess.call(['smrm', '-r', self.test_dir, '--trash_path', self.trash_dir])
        self.assertFalse(os.path.exists(self.test_dir))

        self.logger.info("OK")

    def test_not_recursive(self):
        self.logger.debug('')

        subprocess.call(['smrm', self.test_dir, '--trash_path', self.trash_dir])
        self.assertTrue(os.path.exists(self.test_dir))

        self.logger.info("OK")

    def test_forced(self):
        self.logger.debug('')

        output = subprocess.check_output(['smrm', 'q', '-f', '--trash_path', self.trash_dir])
        self.assertEqual('', output)

        self.logger.info("OK")

    def test_not_forced(self):
        self.logger.debug('')

        output = subprocess.check_output(['smrm', 'q', '--trash_path', self.trash_dir])
        self.assertTrue('No such file or directory' in output)

        self.logger.info("OK")

if __name__ == '__main__':
    unittest.main(verbosity=2)