import unittest
import os
import logging
import shutil
import smartrm.handlers as hndl
from smartrm.trash import Trash
from smartrm.file import File


class FlagsTestClass(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        FORMAT = "%(asctime)s - %(levelname)-8s: %(filename)-10s: %(funcName)s:  %(message)s "
        logging.basicConfig(format=FORMAT, filename="/home/lera/Documents/pythonlabs/smartrm/tests.log",
                            level=logging.DEBUG)
        cls.logger = logging.getLogger()
        cls.logger.debug('')

        cls.logger.info("OK")

    def test_is_file_func_y(self):
        self.logger.debug('')

        with open('file', 'w'):
            pass
        file = File('file')
        self.assertTrue(file.is_file())

        self.logger.info("OK")

    def test_is_file_func_n(self):
        self.logger.debug('')

        os.mkdir('tmpdir')
        file = File('tmpdir')
        self.assertFalse(file.is_file())

        self.logger.info("OK")

    def test_is_dir_func_y(self):
        self.logger.debug('')

        os.mkdir('tmpdir')
        file = File('tmpdir')
        self.assertTrue(file.is_dir())

        self.logger.info("OK")

    def test_is_dir_func_n(self):
        self.logger.debug('')

        with open('file', 'w'):
            pass
        file = File('file')
        self.assertFalse(file.is_dir())

        self.logger.info("OK")

    def test_is_link_func_y(self):
        self.logger.debug('')

        with open('f', 'w'):
            pass
        os.symlink('f', 'file')
        os.remove('f')
        file = File('file')
        self.assertTrue(file.is_link())
        os.remove('file')
        self.logger.info("OK")

    def test_is_link_func_n(self):
        self.logger.debug('')

        with open('file', 'w'):
            pass
        file = File('file')
        self.assertFalse(file.is_link())

        self.logger.info("OK")

    def test_is_empty_func_y(self):
        self.logger.debug('')

        os.mkdir('tmpdir')
        file = File('tmpdir')
        self.assertTrue(file.is_empty())

        self.logger.info("OK")

    def test_is_empty_func_n(self):
        self.logger.debug('')

        os.mkdir('tmpdir')
        with open('tmpdir/file', 'w'):
            pass
        file = File('tmpdi/file')
        self.assertFalse(file.is_empty())

        self.logger.info("OK")

    def test_is_exist_y(self):
        self.logger.debug('')

        with open('file', 'w'):
            pass
        file = File('file')
        self.assertTrue(file.is_exist())

        self.logger.info("OK")

    def test_is_exist_n(self):
        self.logger.debug('')

        file = File('file')
        self.assertFalse(file.is_exist())

        self.logger.info("OK")

    def tearDown(cls):
        if os.path.exists('file'):
            os.remove('file')
        if os.path.exists('tmpdir'):
            shutil.rmtree('tmpdir')

if __name__ == '__main__':
    unittest.main(verbosity=2)