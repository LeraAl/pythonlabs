import unittest
import os
import logging
import shutil
import subprocess
import smartrm.handlers as hndl
from smartrm.trash import Trash


class FlagsTestClass(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        FORMAT = "%(asctime)s - %(levelname)-8s: %(filename)-10s: %(funcName)s:  %(message)s "
        logging.basicConfig(format=FORMAT, filename="/home/lera/Documents/pythonlabs/smartrm/tests.log",
                            level=logging.DEBUG)
        cls.logger = logging.getLogger()
        cls.logger.debug('')

        cls.trash_dir = os.path.join(os.getcwd(), 'test_trash')
        cls.test_dir = os.path.join(os.getcwd(), 'test_tempdir')
        cls.logger.info("OK")

    def setUp(self):
        self.logger.debug('')

        os.mkdir(self.test_dir)
        os.chdir(self.test_dir)
        with open('f2', 'w'):
            pass
        with open('f3', 'w'):
            pass
        with open('f', 'w'):
            pass
        os.chdir('../')

        self.logger.info("OK")

    def tearDown(self):
        self.logger.debug('')

        if os.getcwd() == self.test_dir:
            os.chdir('../')

        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)
        if os.path.exists(self.trash_dir):
            shutil.rmtree(self.trash_dir)

        self.logger.info("OK")

    def test_regex_one_any_symbol(self):
        self.logger.debug('')

        os.chdir(self.test_dir)
        subprocess.call(['smrm', '--regex', 'f.', '--trash_path', self.trash_dir])
        self.assertEqual(['f'], os.listdir('./'))
        os.chdir('../')

        self.logger.info("OK")

    def test_regex_all_files(self):
        self.logger.debug('')

        os.chdir(self.test_dir)
        subprocess.call(['smrm', '--regex', '.*', '--trash_path', self.trash_dir])
        self.assertFalse(os.listdir('./'))
        os.chdir('../')

        self.logger.info("OK")

    def test_regex_more_than_one(self):
        self.logger.debug('')

        os.chdir(self.test_dir)
        with open('q1', 'w'):
            pass
        with open('q', 'w'):
            pass
        subprocess.call(['smrm', '--regex', 'f.*', 'q.', '--trash_path', self.trash_dir])
        self.assertEqual(['q'], os.listdir('./'))
        os.chdir('../')

        self.logger.info("OK")

    def test_regex_rm_dir(self):
        self.logger.debug('')

        os.chdir(self.test_dir)
        os.mkdir('q')
        subprocess.call(['smrm', '--regex', 'q', '--trash_path', self.trash_dir])
        self.assertFalse(os.path.exists('q'))
        os.chdir('../')

        self.logger.info("OK")


if __name__ == '__main__':
    unittest.main(verbosity=2)