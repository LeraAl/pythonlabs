import unittest
import os
import sys
import logging
import shutil
import smartrm.handlers as hndl
from datetime import timedelta
from smartrm.trash import Trash
from smartrm.file import File


class RmFunctionsTestClass(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        FORMAT = "%(asctime)s - %(levelname)-8s: %(filename)-10s: %(funcName)s:  %(message)s "
        logging.basicConfig(format=FORMAT, filename="/home/lera/Documents/pythonlabs/smartrm/tests.log",
                            level=logging.DEBUG)
        cls.logger = logging.getLogger()
        cls.logger.debug('')

        cls.trash_dir = os.path.join(os.getcwd(), 'test_trash')
        cls.test_dir = os.path.join(os.getcwd(), 'test_tempdir')

        cls.logger.info("OK")

    def setUp(self):
        self.logger.debug('')

        os.mkdir(self.test_dir)
        with open('f3', 'w'):
            pass

        self.logger.info("OK")

    def tearDown(self):
        self.logger.debug('')

        if os.path.exists(os.path.abspath(self.test_dir)):
            shutil.rmtree(os.path.abspath(self.test_dir))
        if os.path.exists('f3'):
            os.chmod('f3', 777)
            os.remove('f3')
        shutil.rmtree(os.path.abspath(self.trash_dir))

        self.logger.info("OK")

    def test_remove_symlink(self):
        self.logger.debug('')

        os.symlink('f3', 'link')
        trash = Trash(self.trash_dir, logger=self.logger)
        trash.remove_file(File('link'))
        self.assertFalse(os.path.exists('link'))

        self.logger.info('OK')

    def test_remove_file(self):
        self.logger.debug('')

        trash = Trash(self.trash_dir, logger=self.logger)
        trash.remove_file(File('f3'))
        self.assertFalse(os.path.exists('f3'))

        self.logger.info("OK")

    def test_remove_dir(self):
        self.logger.debug('')

        trash = Trash(self.trash_dir, logger=self.logger)
        os.mkdir('to_delete')
        trash.remove_dir(File('to_delete'))
        self.assertFalse(os.path.exists('to_delete'))

        self.logger.info("OK")

    def test_show(self):
        self.logger.debug('')

        output = []

        def output_msg(msg):
            output.append(msg)

        trash = Trash(self.trash_dir, output_function=output_msg, logger=self.logger)
        trash.remove(['f3'])
        trash.show()

        self.assertEqual(output, ['f3'])

        self.logger.info("OK")

    def test_clean_all(self):
        self.logger.debug('')

        trash = Trash(self.trash_dir, logger=self.logger)
        trash.remove(['f3'])

        trash.clean(clean_all=True)

        self.assertFalse(os.listdir(os.path.join(self.trash_dir, 'files')))

        self.logger.info("OK")

    def test_clean_by_size(self):
        self.logger.debug('')

        trash = Trash(self.trash_dir, size_to_clean=0, logger=self.logger)
        with open('f4', 'w') as f:
            f.write('qwerty')
        with open('f5', 'w') as f:
            f.write('qwerty')
        with open('f6', 'w') as f:
            f.write('qwerty')
        trash.remove(['f3', 'f4', 'f5', 'f6'])
        trash.clean()

        self.assertFalse(os.listdir(os.path.join(self.trash_dir, 'files')))

        self.logger.info("OK")

    def test_clean_by_time(self):
        self.logger.debug('')

        trash = Trash(self.trash_dir, time_before_cleaning=timedelta(microseconds=1), logger=self.logger)
        with open('f4', 'w') as f:
            f.write('qwerty')
        with open('f5', 'w') as f:
            f.write('qwerty')
        with open('f6', 'w') as f:
            f.write('qwerty')
        import time
        trash.remove(['f3', 'f4', 'f5', 'f6'])
        trash.clean()

        self.assertFalse(os.listdir(os.path.join(self.trash_dir, 'files')))

        self.logger.info("OK")

    def test_restore(self):
        self.logger.debug('')

        trash = Trash(self.trash_dir, logger=self.logger)
        trash.remove(['f3'])
        trash.restore(['f3'])

        self.assertTrue(os.path.exists('./f3'))

        self.logger.info("OK")

    @classmethod
    def tearDownClass(cls):
        if os.path.exists('smartrm.egg-info'):
            shutil.rmtree('smartrm.egg-info')


if __name__ == '__main__':
    unittest.main(verbosity=2)