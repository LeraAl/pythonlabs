import unittest
import os
import logging
import shutil
import smartrm.handlers as hndl
from smartrm.trash import Trash


class FlagsTestClass(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        FORMAT = "%(asctime)s - %(levelname)-8s: %(filename)-10s: %(funcName)s:  %(message)s "
        logging.basicConfig(format=FORMAT, filename="/home/lera/Documents/pythonlabs/smartrm/tests.log",
                            level=logging.DEBUG)
        cls.logger = logging.getLogger()
        cls.logger.debug('')

        cls.trash_dir = os.path.join(os.getcwd(), 'test_trash')
        cls.test_dir = os.path.join(os.getcwd(), 'test_tempdir')
        cls.logger.info("OK")

    def setUp(self):
        self.logger.debug('')

        os.mkdir(self.test_dir)
        os.chdir(self.test_dir)
        with open('f1', 'w'):
            pass
        with open('f2', 'w'):
            pass
        os.chdir('../')

        self.logger.debug('end setUp: {} {}'.format(os.getcwd(), os.listdir('./')))
        self.logger.info("OK")

    def tearDown(self):
        self.logger.debug('')

        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)
        shutil.rmtree(self.trash_dir)

        self.logger.info("OK")

    def test_recursive(self):
        self.logger.debug('')

        trash = Trash(self.trash_dir, confirm_go_into=lambda x: True, logger=self.logger)
        trash.remove([self.test_dir])
        self.assertFalse(os.path.exists(self.test_dir))

        self.logger.info("OK")

    def test_recursive_dirs(self):
        self.logger.debug('')

        print os.getcwd()
        print os.listdir('./')
        os.mkdir(os.path.join(self.test_dir, 'qwerty'))
        trash = Trash(self.trash_dir, confirm_go_into=lambda x: True, logger=self.logger)
        trash.remove([self.test_dir])
        self.assertFalse(os.path.exists(self.test_dir))

        self.logger.info("OK")

    @unittest.expectedFailure
    def test_not_recursive(self):
        self.logger.debug('')

        trash = Trash(self.trash_dir, confirm_go_into=lambda x: False, logger=self.logger)
        trash.remove([self.test_dir])
        self.assertTrue(os.path.exists(self.test_dir))

        self.logger.info("OK")

    def test_forced(self):
        self.logger.debug('')

        trash = Trash(self.trash_dir, confirm_go_into=lambda x: True, logger=self.logger, error_handler=hndl.error_handler_forced)
        trash.remove([self.test_dir, 'q', 'w'])
        self.assertFalse(os.path.exists(self.test_dir))

        self.logger.info("OK")

    @unittest.expectedFailure
    def test_not_forced(self):
        self.logger.debug('')

        trash = Trash(self.trash_dir, confirm_go_into=lambda x: True, logger=self.logger)
        trash.remove([self.test_dir, 'q', 'w'])

        self.logger.info("OK")

    @classmethod
    def tearDownClass(cls):
        if os.path.exists('smartrm.egg-info'):
            shutil.rmtree('smartrm.egg-info')

if __name__ == '__main__':
    unittest.main(verbosity=2)