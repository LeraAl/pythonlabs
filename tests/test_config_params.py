import unittest
import os
import logging
import shutil
import subprocess
import smartrm.handlers as hndl
from smartrm.trash import Trash


class FlagsTestClass(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        FORMAT = "%(asctime)s - %(levelname)-8s: %(filename)-10s: %(funcName)s:  %(message)s "
        logging.basicConfig(format=FORMAT, filename="/home/lera/Documents/pythonlabs/smartrm/tests.log",
                            level=logging.DEBUG)
        cls.logger = logging.getLogger()
        cls.logger.debug('')

        cls.trash_dir = os.path.join(os.getcwd(), 'test_trash')
        cls.test_dir = os.path.join(os.getcwd(), 'test_tempdir')
        cls.logger.info("OK")

    def setUp(self):
        self.logger.debug('')

        os.mkdir(self.test_dir)

        self.logger.info("OK")

    def tearDown(self):
        self.logger.debug('')

        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)
        if os.path.exists(self.trash_dir):
            shutil.rmtree(self.trash_dir)

        self.logger.info("OK")

    def test_trash_path(self):
        self.logger.debug('')

        subprocess.call(['smrm', self.test_dir, '--trash_path', self.trash_dir])
        self.assertFalse(os.path.exists(self.test_dir))

        self.logger.info("OK")

    def test_clean_time(self):
        self.logger.debug('')

        subprocess.call(['smrm', self.test_dir, '--trash_path', self.trash_dir])
        subprocess.check_output(['smtrash', '--trash_path', self.trash_dir, '--time_to_clean', '0d 0h 0m', 'clean'])
        output = subprocess.check_output(['smtrash', '--trash_path', self.trash_dir, 'show'])
        self.assertEqual('', output)

        self.logger.info("OK")

    def test_clean_size(self):
        self.logger.debug('')

        subprocess.call(['smrm', self.test_dir, '--trash_path', self.trash_dir])
        subprocess.check_output(['smtrash', '--trash_path', self.trash_dir, '--size_to_clean', '-1', 'clean'])
        output = subprocess.check_output(['smtrash', '--trash_path', self.trash_dir, 'show'])
        self.assertEqual('', output)

        self.logger.info("OK")

    def test_replace_when_remove(self):
        self.logger.debug('')

        subprocess.call(['smrm', self.test_dir, '--trash_path', self.trash_dir])
        os.mkdir(self.test_dir)
        subprocess.call(['smrm', self.test_dir, '--replace_when_remove', 'yes', '--trash_path', self.trash_dir])

        output = subprocess.check_output(['smtrash', '--trash_path', self.trash_dir, 'show']).strip()
        self.assertEqual('test_tempdir', output)

        self.logger.info("OK")

    def test_config_file(self):
        self.logger.debug('')

        with open('config', 'w') as f:
            f.write('ReplaceWhenRemove=Yes')

        subprocess.call(['smrm', self.test_dir, '--trash_path', self.trash_dir])
        os.mkdir(self.test_dir)
        subprocess.call(['smrm', self.test_dir, '--trash_path', self.trash_dir, '--config', 'config'])
        output = subprocess.check_output(['smtrash', '--trash_path', self.trash_dir, 'show']).strip()
        os.remove('config')
        self.assertEqual('test_tempdir', output)

        self.logger.info("OK")


if __name__ == '__main__':
    unittest.main(verbosity=2)