import unittest
import os
import logging
import shutil
import smartrm.handlers as hndl
from smartrm.trash import Trash


class ExceptionsTestClass(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        FORMAT = "%(asctime)s - %(levelname)-8s: %(filename)-10s: %(funcName)s:  %(message)s "
        logging.basicConfig(format=FORMAT, filename="/home/lera/Documents/pythonlabs/smartrm/tests.log",
                            level=logging.DEBUG)
        cls.logger = logging.getLogger()
        cls.logger.debug('')

        cls.trash_dir = os.path.join(os.getcwd(), 'test_trash')
        cls.test_dir = os.path.join(os.getcwd(), 'test_tempdir')

        cls.logger.info("OK")


    def setUp(self):
        self.logger.debug('')

        os.mkdir(self.test_dir)
        os.chdir(self.test_dir)
        with open('f1', 'w'):
            pass
        with open('f2', 'w'):
            pass
        os.chdir('../')
        with open('f3', 'w'):
            pass

        self.logger.debug('end setUp: {} {}'.format(os.getcwd(), os.listdir('./')))
        self.logger.info("OK")

    def tearDown(self):
        self.logger.debug('')

        if os.path.exists(os.path.abspath(self.test_dir)):
            shutil.rmtree(os.path.abspath(self.test_dir))
        if os.path.exists('f3'):
            os.chmod('f3', 777)
            os.remove('f3')
        shutil.rmtree(os.path.abspath(self.trash_dir))

        self.logger.info("OK")

    @unittest.expectedFailure
    def test_remove_trash_dir_error(self):
        self.logger.debug('')

        trash = Trash(self.trash_dir, logger=self.logger)
        trash.remove([self.trash_dir])

        self.logger.info("OK")

    @unittest.expectedFailure
    def test_remove_non_existing_file(self):
        self.logger.debug('')

        trash = Trash(self.trash_dir, logger=self.logger)
        trash.remove([1, 2])

        self.logger.info("OK")

    @unittest.expectedFailure
    def test_remove_not_empty_dir(self):
        self.logger.debug('')

        trash = Trash(self.trash_dir, logger=self.logger)
        trash.remove([self.test_dir])

        self.logger.info("OK")

    @unittest.expectedFailure
    def test_permission_denied_error(self):
        self.logger.debug('')

        trash = Trash(self.trash_dir, logger=self.logger)
        os.chmod('f3', 000)
        trash.remove([self.test_dir])

        self.logger.info("OK")

    @classmethod
    def tearDownClass(cls):
        if os.path.exists('smartrm.egg-info'):
            shutil.rmtree('smartrm.egg-info')


if __name__ == '__main__':
    unittest.main(verbosity=2)